Loading fsl_mrsi Results
========================

The `FSL-MRS <https://git.fmrib.ox.ac.uk/fsl/fsl_mrs>`_ package tool |fsl_mrsi|_ fits MRSI data and outputs a folder structure containing:

* the spectral fits (fit, baseline, and residual),
* the (internally or water) scaled concentrations of each fitted and combined metabolite,
* the fitting uncertainties for each metabolite and combined metabolite,
* SNR and linewidth (FWHM) QC metrics for each metabolite.

This plugin provides an easy to use interface to load and view each of these outputs.

.. _fsl_mrsi: https://open.win.ox.ac.uk/pages/fsl/fsl_mrs/fitting.html#mrsi

.. |fsl_mrsi| replace:: *fsl_mrsi*

Loading the data
----------------

After loading some MRSI data (see :ref:`loading_data`) load the results by selecting the `Load FSL-MRS fit` in the `Tools` menu.

.. image:: img/fsl_mrsi_menu.png
    :width: 400
    :align: center

Use the dialogue that opens to select the results directory produced by fsl_mrsi (it should contain `concs`, `fit`, `qc`, and `uncertainties` sub-directories).

.. image:: img/fsl_mrsi_load.png
    :width: 400
    :align: center

The results data will now be loaded.

Fit
---
After the data has loaded the FSL-MRS fit will be displayed, in red, on top of the spectrum in the `MRS View` panel.

.. image:: img/mrsi_fit.png

Additionally the baseline (green) and residual (grey) will be displayed. Each line should update as the cursor is moved over each voxel in the orthographic view.


Metabolite and QC maps
----------------------

The `Load FSL-MRS fit` action will also create a `FSL-MRS Results` panel adjacent to the orthographic view panel.

.. image:: img/results_panel.png
    :width: 400
    :align: center

This panel allows the user to quickly load metabolite maps. For example selecting *NAA+NAAG* in the Metabolite drop-down and *conc-internal* will load the metabolite map of NAA+NAAG concentrations referenced to total creatine.

.. image:: img/metab_map_1.png
    :width: 600
    :align: center

Selecting a different map (*Cr* & *FWHM*) will remove the previous metabolite map and load the new one. If you want to retain the previous map, untick **Replace?** before selecting a new map. Overlays can also be managed through the `Overlay list` panel.

.. image:: img/metab_map_2.png
    :width: 600
    :align: center