Viewing NIfTI-MRS Files
=======================

.. _loading_data:

Loading the Plugin
------------------
To enable the MRS components in FSLeyes, select the MRS layout from the `Layouts` submenu of the `View` menu.

.. image:: img/view_layouts_mrs.png
    :width: 200
    :align: center

Alternatively start fsleyes with the `-smrs` flag, or the `--showAllPlugins` flag.
    
    fsleyes -smrs ...

    fsleyes --showAllPlugins ...

Any of the above options will enable the `MRS View` option in the `View` menu, and the `Load FSL-MRS Fit` in the `Tools` menu.

The MRS View
------------

NIfTI-MRS files are handled natively as complex NIfTI files by FSLeyes. This plugin introduces a dedicated viewer for MRS and the NIfTI-MRS format which aims to ease the viewing of such data and introduces additional features not available in the base version.

Start the dedicated MRS View by selecting `MRS view` in the `View` menu.

.. image:: img/mrs_view_menu_item.png
    :width: 400
    :align: center

This will open a dedicated MRS plotting panel in FSLeyes.

.. image:: img/mrs_view_open.png

A spectrum will be displayed when activated in the overlay list and the correct location is selected in the orthographic view.

.. image:: img/mrs_view_data.png

In the MRS View spectra should appear correctly orientated and with the correct chemical shift scale.

.. |mrs_ctrl_icon| image:: img/mrs_ctrl_panel_icon.png
    :width: 24

By default the real component of the spectrum is displayed. A control panel for the view (open with |mrs_ctrl_icon|) allows the user to select other views (imaginary/magnitude/phase) as well as apply basic phasing. The line colour, line type and plot background can also be customised in this menu.

.. image:: img/mrs_ctrl_panel.png
    :width: 300
    :align: center

A built in MRS layout can be loaded from the command line using:
    
    fsleyes -smrs ...

If this is done with data specified, e.g. the example data found `here <https://github.com/wexeee/mrs_nifti_standard/tree/master/example_data/svs_1>`_

    fsleyes -smrs T1.nii.gz svs_preprocessed.nii.gz

then a view like this should be generated.

.. image:: img/mrs_view_cmdline.png

*Note that the overlay 'svs_preprocessed' has been selected in the overlay list and the voxel location selected in the ortho panel.*

Viewing higher dimensions in NIfTI-MRS
--------------------------------------

NIfTI-MRS has the ability to display data contained in the higher (5th, 6th, and 7th) dimensions of a NIfTI file. NIfTI-MRS uses these dimensions to store things like uncombined coils, repeated averages (dynamics), or editing sub-spectra.

.. |mrs_icon| image:: ../fsleyes_plugin_mrs/icons/nifti_mrs_icon-mrs_icon_highlight_thumb24.png

This functionality is provided using the NIfTI-MRS panel. This opens by default with the MRS View, but can also be accessed using the |mrs_icon| icon or through the menus.

.. image:: img/nifti_mrs_menu.png
    :width: 400
    :align: center

For all data this panel provides basic information about the spectrum currently selected in the overlay list.

.. image:: img/nifti_mrs_basic_info.png
    :width: 300
    :align: center

If the data uses the higher (5th, 6th or 7th) NIfTI-MRS encoding dimensions the plugin will add sliders for looping through the data. Here a `synthetic MEGA-PRESS spectrum <https://github.com/wexeee/mrs_nifti_standard/blob/master/example_data/dynamic_header_example/example_1.nii.gz>`_ is displayed.

.. image:: img/nifti_mrs_more_1.png

This (noiseless synthetic) data actually uses all three higher dimensions to store uncombined coils, repeated dynamics, and the editing dimension. As a result two things happen in the NIfTI-MRS panel. First the information is expanded with information about the dimensions and any dynamically changing header data. Second, sliders are added to enable the user to view different indices of the higher dimensions.

.. image:: img/nifti_mrs_more_info.png
    :width: 300
    :align: center

We can view different indices simultaneously by using the *Plot List* panel. Clicking the '+' button fixes the current view, enabling us to change the slider location whilst also retaining the current spectrum.
In this data we can view the two editing conditions stored in the final (7th) dimension.

.. image:: img/nifti_mrs_more_2.png


Averaging and differencing
~~~~~~~~~~~~~~~~~~~~~~~~~~

In version `0.1.0` check boxes were added to average or difference the data along a dimension. The latter is only available when there are two elements. This can be useful for quickly looking at the quality fo the data before processing, or looking at edited data. Here we have a MEGA-edited sLASER acquisition. Looking without averaging you can't see much data.

.. image:: img/avg_diff_0_small.png

If we tick the `Average DIM 6` button then we will see all the elements of the `DIM_DYN` dimension averaged.

.. image:: img/avg_diff_1_small.png

Now we can have a quick look at the difference spectrum by ticking the `Difference DIM 5` button. This is the `DIM_EDIT` dimension and only has two elements. Note that the iamge shown below has also be rephased using the tools in Fsleyes.

.. image:: img/avg_diff_2_small.png


Viewing time domain data
------------------------
It is possible to view the time domain data by using the Time Series view panel.

.. image:: img/time_series_menu.png
    :width: 400
    :align: center

Currently, higher NIfTI-MRS dimensions are not handled in this view.

.. image:: img/time_series.png
